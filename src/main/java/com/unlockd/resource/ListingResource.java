package com.unlockd.resource;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.unlockd.client.RedditClient;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Collections;
import java.util.List;

@Path("/listings")
@Produces(MediaType.APPLICATION_JSON)
public class ListingResource {
  private final RedditClient client;

  @Inject
  public ListingResource(final RedditClient client) {
    this.client = client;
  }

  @GET
  @Timed
  public List<String> listings() {
    return Collections.emptyList();
    //return client.titles();
  }
}
