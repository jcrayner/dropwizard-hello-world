package com.unlockd.resource;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.unlockd.model.Saying;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@Path("/hello-world")
@Produces(MediaType.APPLICATION_JSON)
public class HelloWorldResource {
  private final String template;
  private final String defaultName;
  private final AtomicLong counter;

  @Inject
  public HelloWorldResource(@Named("template") final String template, @Named("defaultName") final String defaultName) {
    this.template    = template;
    this.defaultName = defaultName;
    this.counter     = new AtomicLong();
  }

  @GET
  @Timed
  public Saying sayHello(@QueryParam("name") final String name) {
    return new Saying(counter.incrementAndGet(), String.format(template, Optional.ofNullable(name).orElse(defaultName)));
  }
}
