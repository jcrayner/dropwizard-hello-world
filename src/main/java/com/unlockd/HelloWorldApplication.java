package com.unlockd;

import com.unlockd.configuration.HelloWorldConfiguration;
import com.unlockd.module.HelloWorldModule;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.vyarus.dropwizard.guice.GuiceBundle;

import java.lang.management.ManagementFactory;

public class HelloWorldApplication extends Application<HelloWorldConfiguration> {
  private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldApplication.class);

  public static void main(String[] args) throws Exception {
    final Application<HelloWorldConfiguration> application = new HelloWorldApplication();
    Runtime.getRuntime().addShutdownHook(
        new Thread(
            () -> {
              LOGGER.info(
                  "Shutting down {} running on {}",
                  application.getName(),
                  ManagementFactory.getRuntimeMXBean().getName()
              );
            },
            "shutdown-hook"
        )
    );

    application.run(args);
  }

  @Override
  public String getName() {
    return "hello-world";
  }

  @Override
  public void initialize(Bootstrap<HelloWorldConfiguration> bootstrap) {
    bootstrap.addBundle(
        GuiceBundle.<HelloWorldConfiguration>builder()
            .enableAutoConfig("com.unlockd")
            .modules(new HelloWorldModule())
            .printDiagnosticInfo()
            .build()
    );
  }

  @Override
  public void run(HelloWorldConfiguration configuration, Environment environment) throws Exception {
  }
}
