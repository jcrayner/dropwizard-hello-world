package com.unlockd.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.Objects;

public class RedditClientConfiguration {
  @NotEmpty
  private final String scheme;
  @NotEmpty
  private final String host;
  @NotEmpty
  private final String path;

  public RedditClientConfiguration(
      @JsonProperty("scheme") final String scheme,
      @JsonProperty("host") final String host,
      @JsonProperty("path") final String path
  ) {
    this.scheme = scheme;
    this.host = host;
    this.path = path;
  }

  public String getScheme() {
    return scheme;
  }

  public String getHost() {
    return host;
  }

  public String getPath() {
    return path;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    RedditClientConfiguration that = (RedditClientConfiguration) o;
    return Objects.equals(scheme, that.scheme) &&
        Objects.equals(host, that.host) &&
        Objects.equals(path, that.path);
  }

  @Override
  public int hashCode() {
    return Objects.hash(scheme, host, path);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("scheme", scheme)
        .add("host", host)
        .add("path", path)
        .toString();
  }
}
