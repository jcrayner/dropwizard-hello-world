package com.unlockd.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class Saying {
  @NotNull
  private final Long id;
  @NotNull
  @NotEmpty
  private final String content;

  @JsonCreator
  public Saying(@JsonProperty(value = "id") Long id, @JsonProperty(value = "content") String content) {
    this.id = id;
    this.content = content;
  }

  @JsonProperty
  public Long getId() {
    return id;
  }

  @JsonProperty
  public String getContent() {
    return content;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Saying saying = (Saying) o;
    return Objects.equals(id, saying.id) &&
        Objects.equals(content, saying.content);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, content);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("id", id)
        .add("content", content)
        .toString();
  }
}
