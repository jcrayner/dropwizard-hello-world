package com.unlockd.module;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.github.bohnman.squiggly.Squiggly;
import com.github.bohnman.squiggly.web.RequestSquigglyContextProvider;
import com.github.bohnman.squiggly.web.SquigglyRequestFilter;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.unlockd.client.DefaultRedditClient;
import com.unlockd.client.RedditClient;
import com.unlockd.configuration.HelloWorldConfiguration;
import com.unlockd.configuration.RedditClientConfiguration;
import io.dropwizard.setup.Environment;
import org.glassfish.jersey.client.ClientConfig;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

public class HelloWorldModule extends AbstractModule {
  @Named("template")
  @Provides
  String template(HelloWorldConfiguration configuration) {
    return configuration.getTemplate();
  }

  @Named("defaultName")
  @Provides
  String defaultName(HelloWorldConfiguration configuration) {
    return configuration.getDefaultName();
  }

  @Provides
  ObjectMapper objectMapper(Environment environment) {
    return environment.getObjectMapper();
  }

  @Provides
  RedditClientConfiguration redditClientConfiguration(HelloWorldConfiguration configuration) {
    return configuration.getReddit();
  }

  @Provides
  Client httpClient(ObjectMapper mapper) {
    ClientConfig config = new ClientConfig();
    config.register(new SquigglyRequestFilter());

    Squiggly.init(mapper, new RequestSquigglyContextProvider());
    JacksonJaxbJsonProvider jacksonJaxbJsonProvider = new JacksonJaxbJsonProvider();
    jacksonJaxbJsonProvider.setMapper(mapper);
    config.register(jacksonJaxbJsonProvider);

    return ClientBuilder.newClient(config);
  }

  @Override
  protected void configure() {
    bind(RedditClient.class).to(DefaultRedditClient.class);
  }
}
