package com.unlockd.client;

import java.util.List;

public interface RedditClient {
  List<String> titles();
}
