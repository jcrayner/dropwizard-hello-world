package com.unlockd.client;

import com.google.inject.Inject;
import com.unlockd.configuration.RedditClientConfiguration;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.util.List;

public class DefaultRedditClient implements RedditClient {
  private final Client delegate;
  private final RedditClientConfiguration configuration;

  @Inject
  public DefaultRedditClient(RedditClientConfiguration configuration, Client client) {
    this.delegate = client;
    this.configuration = configuration;
  }

  public List<String> titles() {
    return delegate.target(
            UriBuilder.fromPath(configuration.getPath())
              .scheme(configuration.getScheme())
              .host(configuration.getHost())
              .build()
        )
        .request(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .buildGet()
        .invoke(new GenericType<List<String>>() {});
  }
}
