package com.unlockd.health;

import com.codahale.metrics.health.HealthCheck;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import ru.vyarus.dropwizard.guice.module.installer.feature.health.NamedHealthCheck;

public class TemplateHealthCheck extends NamedHealthCheck {
  private final String template;

  @Inject
  public TemplateHealthCheck(@Named("template") final String template) {
    this.template = template;
  }

  @Override
  protected Result check() throws Exception {
    final String resolved = String.format(template, "TEST");

    if(!resolved.contains("TEST")) {
      return Result.unhealthy("Template does not allow for name interpolation");
    }

    return Result.healthy();
  }

  @Override
  public String getName() {
    return "Template Health Check";
  }
}
