package com.unlockd.health;

import org.hamcrest.beans.HasPropertyWithValue;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class TemplateHealthCheckTest {
  @Test
  public void testThatAnInvalidTemplateResultsInAnUnhealthyResponse() throws Exception {
    assertThat(
        new TemplateHealthCheck("interpolate me").execute(),
        HasPropertyWithValue.hasProperty("healthy", is(false))
    );
  }

  @Test
  public void testThatAValidTemplateResultsInAHealthyResponse() throws Exception {
    assertThat(
        new TemplateHealthCheck("interpolate me, %s").execute(),
        HasPropertyWithValue.hasProperty("healthy", is(true))
    );
  }
}
