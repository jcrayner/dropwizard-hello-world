package com.unlockd.resource;

import com.unlockd.model.Saying;
import com.unlockd.AbstractApplicationTest;
import org.hamcrest.beans.HasPropertyWithValue;
import org.junit.Test;

import javax.ws.rs.core.MediaType;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class HelloWorldResourceTest extends AbstractApplicationTest {
  @Test
  public void testHelloWorldWithAProvidedName() throws Exception {
    assertThat(
        newClient().target(uriForResource(HelloWorldResource.class))
            .queryParam("name", "jeremy")
            .request(MediaType.APPLICATION_JSON_TYPE)
            .accept(MediaType.APPLICATION_FORM_URLENCODED_TYPE)
            .buildGet()
            .invoke(Saying.class),
        HasPropertyWithValue.hasProperty("content", equalTo("Hello, jeremy"))
    );
  }

  @Test
  public void testHelloWorldWithNoNameProvided() throws Exception {
    assertThat(
        newClient().target(uriForResource(HelloWorldResource.class))
            .request(MediaType.APPLICATION_JSON_TYPE)
            .accept(MediaType.APPLICATION_FORM_URLENCODED_TYPE)
            .buildGet()
            .invoke(Saying.class),
        HasPropertyWithValue.hasProperty("content", equalTo("Hello, Stranger"))
    );
  }
}
