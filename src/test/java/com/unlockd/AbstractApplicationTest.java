package com.unlockd;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.unlockd.configuration.HelloWorldConfiguration;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.uri.internal.JerseyUriBuilder;
import org.junit.ClassRule;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.UriBuilder;
import java.lang.reflect.Method;

abstract public class AbstractApplicationTest {
  private static final ObjectMapper MAPPER = Jackson.newObjectMapper()
      .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
      .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
      .configure(MapperFeature.USE_GETTERS_AS_SETTERS, false);

  @ClassRule
  public static final DropwizardAppRule<HelloWorldConfiguration> APPLICATION =
      new DropwizardAppRule<HelloWorldConfiguration>(
          HelloWorldApplication.class,
          ResourceHelpers.resourceFilePath("test-config.yml")
      );


  public static Client newClient() {
    ClientConfig config = new ClientConfig();
    JacksonJaxbJsonProvider jacksonJaxbJsonProvider = new JacksonJaxbJsonProvider();
    jacksonJaxbJsonProvider.setMapper(MAPPER);
    config.register(jacksonJaxbJsonProvider);

    return ClientBuilder.newClient(config);
  }

  public static UriBuilder uriForResource(Class<?> resourceClass) {
    return JerseyUriBuilder.fromResource(resourceClass)
        .scheme("http")
        .host("localhost")
        .port(APPLICATION.getLocalPort());
  }

  public static UriBuilder uriForMethod(Class<?> resourceClass, String method) {
    return uriForResource(resourceClass).path(resourceClass, method);
  }

  public static UriBuilder uriForMethod(Method method) {
    return uriForResource(method.getDeclaringClass()).path(method);
  }
}
