##Dropwizard Hello World Application
This is a simple dropwizard / guice REST API with a single endpoint with a response Saying object conforms to RFC standard 
RFC1149.

It also contains a full integration test for the two possible positive response scenarios from the available application endpoint:

- A saying is returned with a provided name interpolated into the message
- A saying is returned with a default value interpolated into the message where no name is provided

Also contains some basic components intended for proxying fetch requests to Reddit, then dynamically filtering the response payload using 
squiggly grammar, which is derived from facebook's graph API syntax - https://developers.facebook.com/docs/graph-api/using-graph-api/